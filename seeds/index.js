const mongoose = require('mongoose');
const db = mongoose.connection;
const Campground = require('../models/campground');
const cities = require('./cities');
const {places, descriptors} = require('./seedHelpers');

/* Start mongo */
mongoose.connect('mongodb://localhost:27017/yelpcamp',
    { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true }).then(() => {
        console.log("Database Connected.....")
    })
    .catch(err => {
        console.log("Database connection failure....")
        console.log(err)
    });

const sample = array => array[Math.floor(Math.random() * array.length)];

const seedDB = async () => {
    await Campground.deleteMany({});

    for (let i = 0; i < 50; i++) {
        const random1000 = Math.floor(Math.random() * 1000);
        const campList = new Campground({
            location: `${cities[random1000].city}, ${cities[random1000].state}`,
            title: `${sample(descriptors)} ${sample(places)}`
        })
        await campList.save();
    }


}

seedDB()
.then(() =>
{
    mongoose.connection.close();
})

